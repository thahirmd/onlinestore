/*~
 * Writer Plugin
 *
 * @param id       - the identifier returned by the collector
 * @param envelope - the final envelope
 * @param options  - an object options. Options are sent from Java
 *
 * @return - nothing
 */
function write(id, envelope, options) {
  envData = new NodeBuilder().addNode(fn.head(envelope)).toNode()
  let lId = envData.envelope.instance.Product.sku;
  let lUri = '/'+options.entity+'/'+lId+'.json';
  let lCollections = [options.entity, 'Final'];
  let lPermissions = xdmp.defaultPermissions();
  xdmp.documentInsert(lUri, envelope, lPermissions, lCollections);
}


module.exports = write;
