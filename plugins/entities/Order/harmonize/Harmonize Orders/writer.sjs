/*~
 * Writer Plugin
 *
 * @param id       - the identifier returned by the collector
 * @param envelope - the final envelope
 * @param options  - an object options. Options are sent from Java
 *
 * @return - nothing
 */
function write(id, envelope, options) {
  let lURI = '/'+options.entity+'/'+id+'.json'
  let schemaVersion = envelope.envelope.instance.Order.sourceSchema;
  let ver = fn.replace(schemaVersion, "[.]",'')
  let lCollections = [options.entity, 'Final', 'version'+ver]
  xdmp.documentInsert(lURI, envelope, xdmp.defaultPermissions(), lCollections);
}

module.exports = write;
