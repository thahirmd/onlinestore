/*
 * Create Content Plugin
 *
 * @param id         - the identifier returned by the collector
 * @param options    - an object containing options. Options are sent from Java
 *
 * @return - your content
 */
function createContent(id, options) {
  const jsearch = require('/MarkLogic/jsearch');

  // find every order document with id == the passed in id variable
  var orders = jsearch
    .collections('order')
    .documents()
    .where(
      jsearch.byExample({
        'id': id
      })
    )
    .result('value')
    .results.map(function(doc) {
    return {uri:doc.uri, headers: doc.document.envelope.headers, content: doc.document.envelope.attachments};
    })

  // the original source documents
	var attachments = [];
	var uriLst = [];
	var products = [];
    var sourceSchema;
	var price = 0;
    var discountedPrice =0;
    var discountPercent =0;
    var discountTier = 0;
    var customerId = 0;
    var orderId = parseInt(id);
    
	for (var i = 0; i < orders.length; i++) {
 	 	var order = orders[i];
      sourceSchema = order.headers.sourceSchema;
  	attachments.push(order.content)
      customerId = parseInt(order.content.customer)
  	uriLst.push(makeReferenceObject('uri',order.uri))
  	products.push(makeReferenceObject('Product', order.content.sku));
  	price += xs.decimal(parseFloat(order.content.price)) * xs.decimal(parseInt(order.content.quantity, 10));
      discountedPrice += xs.decimal(parseFloat(order.content.discounted_price)) * xs.decimal(parseInt(order.content.quantity, 10));
	}
  
  discountPercent = Math.round(((price - discountedPrice)/price)*100);
  
  switch (true) {
    case discountPercent ==0:
      discountTier = 0;
      break; 
    case discountPercent <=10:
      discountTier = 4;
      break;
    case discountPercent <=30:
      discountTier = 3;
      break;
    case discountPercent <=60:
      discountTier = 2;
      break; 
    case discountPercent <=80:
      discountTier = 1;
      break;
    default:
      discountTier = 0;
}
  
  // return the instance object
  return {
    '$attachments': attachments,
    '$type': 'Order',
    '$version': '1.0.1',
    'sourceSchema': sourceSchema,
    'orderId': orderId,
    'customerId' : customerId,
    'sources':uriLst,
    'products': products,
    'OrderListTotal' : price,
    'OrderTotal': discountedPrice,
    'discountPercent': discountPercent,
    'discountTier': discountTier
  }
};

function makeReferenceObject(type, ref) {
  return {
    '$type': type,
    '$ref': ref
  };
}

module.exports = {
  createContent: createContent
};


